Building jprefctl from source:
------------------------------

First read the attached LICENSE file before using this software anywhere.

This is a maven project, so you can use maven to create packages from scratch.

If you are using OSX, you can install via Homebrew: `brew tap canbican/jprefctl && brew install jprefctl`

Otherwise, please download the jar file, install it in a your preferred
directory and call it with `java -jar <jarfile> ...`.

--
Can Bican <can@bican.net>
2015/07/27
