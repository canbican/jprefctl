/*
 * This file is part of jprefctl. jprefctl is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. jprefctl is distributed in
 * the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with jprefctl. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.bican.jprefctl;

import gnu.getopt.Getopt;

import java.util.ArrayList;
import java.util.prefs.BackingStoreException;

/**
 * Main class for the application. <br/>
 * See the project web page for command line options.
 * 
 * @author Can Bican
 */
public class Main {
  
  private static final String APPNAME = "jprefctl"; //$NON-NLS-1$
  private static final String CMDLINE_OPTIONS = "ae:ihsw:x:"; //$NON-NLS-1$
  
  /**
   * main function of the application.
   * 
   * @param args
   *          command line options
   * @throws BackingStoreException
   *           when there is an error reading preferences
   * @throws JprefctlSyntaxException
   *           when there is a syntax error in the query
   */
  public static void main(String[] args)
      throws JprefctlSyntaxException, BackingStoreException {
    boolean fromSystem = false;
    ArrayList<String> writeList = new ArrayList<>();
    ArrayList<String> deleteList = new ArrayList<>();
    ArrayList<String> printList = new ArrayList<>();
    boolean showDump = false;
    boolean doImport = false;
    
    Getopt g = new Getopt(APPNAME, args, CMDLINE_OPTIONS);
    int c;
    while ((c = g.getopt()) != -1) {
      switch (c) {
        case 'a':
          showDump = true;
          break;
        case 'e':
          printList.add(g.getOptarg());
          break;
        case 'i':
          doImport = true;
          break;
        case 'h':
          PrefOptions.showHelp();
          break;
        case 's':
          fromSystem = !fromSystem;
          break;
        case 'w':
          writeList.add(g.getOptarg());
          break;
        case 'x':
          deleteList.add(g.getOptarg());
          break;
        case '?':
          break;
        default:
          System.out.printf("getopt() returned %s\n", Integer.valueOf(c)); //$NON-NLS-1$
      }
    }
    if (doImport) {
      Nodes n = new Nodes();
      n.getAll(System.in);
      n.setAll(fromSystem);
    } else if (showDump) {
      Nodes n = new Nodes();
      n.getAll(fromSystem);
      System.out.println(n);
    } else if (!writeList.isEmpty()) {
      Object[] lst = writeList.toArray();
      for (int i = 0; i < lst.length; i++) {
        String entry = (String) lst[i];
        NodeDefinition node = NodeDefinition.fromString(entry);
        node.setPref(fromSystem);
      }
    } else if (!deleteList.isEmpty()) {
      Object[] lst = deleteList.toArray();
      for (int i = 0; i < lst.length; i++) {
        String entry = (String) lst[i];
        NodeDefinition node = NodeDefinition.fromString(entry);
        node.deletePref(fromSystem);
      }
    } else if (!printList.isEmpty()) {
      Object[] lst = printList.toArray();
      for (int i = 0; i < lst.length; i++) {
        String entry = (String) lst[i];
        NodeDefinition node = NodeDefinition.fromString(entry);
        NodeDefinition node2 = NodeDefinition.fromPrefs(node.getTree(),
            node.getKey(), fromSystem);
        System.out.println(node2);
      }
    } else {
      PrefOptions.showHelp();
    }
  }
}
