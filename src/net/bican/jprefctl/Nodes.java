/*
 * This file is part of jprefctl. jprefctl is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. jprefctl is distributed in
 * the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with jprefctl. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.bican.jprefctl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * An array of node definitions.
 * 
 * @author Can Bican
 */
public class Nodes {
  private static final String NL_STRING = "\n"; //$NON-NLS-1$
  private static final String EMPTY_STRING = ""; //$NON-NLS-1$
  private ArrayList<NodeDefinition> list = null;
  
  /**
   * Default constructor.
   */
  public Nodes() {
    this.list = new ArrayList<>();
  }
  
  /**
   * Retrieves all entries of the preferences database.
   * 
   * @param fromSystem
   *          if true, retrieves entries from the system database
   * @throws BackingStoreException
   *           when there is an error reading preferences
   */
  public void getAll(boolean fromSystem) throws BackingStoreException {
    Preferences p = NodeDefinition.getPrefRoot(fromSystem);
    getSubTree(p, fromSystem);
  }
  
  /**
   * Retrieves all entries from a stream.
   * 
   * @param in
   *          input stream to read from
   * @throws JprefctlSyntaxException
   *           when there is a syntax error in the query
   */
  public void getAll(InputStream in) throws JprefctlSyntaxException {
    BufferedReader r = new BufferedReader(new InputStreamReader(in));
    do {
      try {
        String s = r.readLine();
        if (s == null) {
          break;
        }
        if (!EMPTY_STRING.equals(s)) {
          NodeDefinition n = NodeDefinition.fromString(s);
          this.list.add(n);
        }
      } catch (IOException e) {
        break;
      }
    } while (true);
  }
  
  private void getSubTree(Preferences p, boolean fromSystem)
      throws BackingStoreException {
    String[] lst = p.childrenNames();
    for (int i = 0; i < lst.length; i++) {
      String subTree = lst[i];
      Preferences pSub = p.node(subTree);
      getSubTree(pSub, fromSystem);
    }
    String[] lstKeys = p.keys();
    for (int i = 0; i < lstKeys.length; i++) {
      String key = lstKeys[i];
      NodeDefinition n = new NodeDefinition(p.absolutePath(), key,
          p.get(key, EMPTY_STRING));
      this.list.add(n);
    }
  }
  
  /**
   * Writes all its entries in the preferences database.
   * 
   * @param system
   *          if true, writes to the system database
   */
  public void setAll(boolean system) {
    Object[] lst = this.list.toArray();
    for (int i = 0; i < lst.length; i++) {
      NodeDefinition n = (NodeDefinition) lst[i];
      System.out.println(n);
      n.setPref(system);
    }
  }
  
  /**
   * Returns a string representation of the array line by line.
   * 
   * @return a string representation of all entries in the array
   */
  @Override
  public String toString() {
    String result = EMPTY_STRING;
    Object[] lst = this.list.toArray();
    for (int i = 0; i < lst.length; i++) {
      NodeDefinition n = (NodeDefinition) lst[i];
      result += n.toString() + NL_STRING;
    }
    return result;
  }
}
