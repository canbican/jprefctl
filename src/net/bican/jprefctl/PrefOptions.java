/*
 * This file is part of jprefctl. jprefctl is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. jprefctl is distributed in
 * the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with jprefctl. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.bican.jprefctl;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Keeps general options for the application.
 * 
 * @author Can Bican
 */
public class PrefOptions {
  private static final String DELIMITER_KEY = "delimiter"; //$NON-NLS-1$
  private static final String PREF_PREFIX = "net/bican/jprefctl"; //$NON-NLS-1$
  private static String DELIMITER = ":"; //$NON-NLS-1$
  
  /**
   * Returns the default delimiter.
   * 
   * @return ":" by default unless "net/bican/jprefctl/delimiter" entry exists
   *         in the database.
   */
  public static String getDelimiter() {
    String result = DELIMITER;
    Preferences p = Preferences.userRoot();
    try {
      if (p.nodeExists(PREF_PREFIX)) {
        p = p.node(PREF_PREFIX);
        result = p.get(DELIMITER_KEY, DELIMITER);
      }
    } catch (BackingStoreException e) {
      // ignore and return default
    }
    return result;
  }
  
  /**
   * Prints the help screen and exits.
   */
  @SuppressWarnings("nls")
  public static void showHelp() {
    System.out.println("\njprefctl usage: \n" + "\n" + "jprefctl [-s] -a:\n"
        + "  Dump all entries.\n" + "jprefctl [-s] -e <entry> -e <entry> ...\n"
        + "  Dump only given entries.\n" + "jprefctl [-s] -i\n"
        + "  Import entries from stdin.\n"
        + "jprefctl [-s] -w <entry> -w <entry> ...\n"
        + "  Writes entries to the database.\n"
        + "jprefctl [-s] -x <entry> -x <entry> ...\n"
        + "  Deletes entries from the database.\n");
    System.exit(1);
  }
}
